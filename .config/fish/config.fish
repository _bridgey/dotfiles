set -x LANG ja_JP.UTF-8
set -x LC_CTYPE ja_JP.UTF-8
# for btrfs
alias refcp="cp --reflink=auto"

if test -d $HOME/bin/
    set -x PATH $HOME/bin $PATH
end

# linuxbrew
set -x BREW_ROOT /home/linuxbrew/.linuxbrew
if test -d $BREW_ROOT
    set -x PATH $BREW_ROOT/bin $PATH
    set -x MANPATH $BREW_ROOT/share/man $MANPATH
    set -x HOMEBREW_EDITOR vim
    set -x INFOPATH $BREW_ROOT/share/info $INFOPATH
    set -g fish_user_paths $BREW_ROOT/sbin $fish_user_paths
end

# pyenv
set -x PYENV_ROOT $HOME/.pyenv
if test -d $PYENV_ROOT
    set -x PATH $PYENV_ROOT/bin $PATH
    set -x PATH $PYENV_ROOT/shims $PATH
    pyenv init - | source
    pyenv virtualenv-init - | source
end

# go
set -x GOPATH $HOME/.go
if test -d $BREW_ROOT/opt/go/bin
    set -x PATH $BREW_ROOT/opt/go/libexec/bin $PATH
    set -x PATH $GOPATH/bin $PATH
end

# Rust
if test -d $HOME/.cargo/bin
    set -x PATH $HOME/.cargo/bin $PATH
end

# Deno
set -x DENO_INSTALL $HOME/.deno
set -x PATH $DENO_INSTALL/bin $PATH

# plugins
function fish_user_key_bindings
    bind \cr peco_select_history
    bind \cg peco_select_ghq_repository
    # bind \cg 'stty sane; peco_select_ghq_repository'
    bind \cx\cr peco_recentd
end

# decors/fish-ghq
set -U GHQ_SELECTOR peco

# starship
starship init fish | source

# Kanagawa Fish shell theme
# https://github.com/rebelot/kanagawa.nvim/blob/master/extras/kanagawa.fish
# A template was taken and modified from Tokyonight:
# https://github.com/folke/tokyonight.nvim/blob/main/extras/fish_tokyonight_night.fish
set -l foreground DCD7BA normal
set -l selection 2D4F67 brcyan
set -l comment 727169 brblack
set -l red C34043 red
set -l orange FF9E64 brred
set -l yellow C0A36E yellow
set -l green 76946A green
set -l purple 957FB8 magenta
set -l cyan 7AA89F cyan
set -l pink D27E99 brmagenta

# Syntax Highlighting Colors
set -g fish_color_normal $foreground
set -g fish_color_command $cyan
set -g fish_color_keyword $pink
set -g fish_color_quote $yellow
set -g fish_color_redirection $foreground
set -g fish_color_end $orange
set -g fish_color_error $red
set -g fish_color_param $purple
set -g fish_color_comment $comment
set -g fish_color_selection --background=$selection
set -g fish_color_search_match --background=$selection
set -g fish_color_operator $green
set -g fish_color_escape $pink
set -g fish_color_autosuggestion $comment

# Completion Pager Colors
set -g fish_pager_color_progress $comment
set -g fish_pager_color_prefix $cyan
set -g fish_pager_color_completion $foreground
set -g fish_pager_color_description $comment

# Rye
bass source "$HOME/.rye/env"

# mise located at ~/.cargo/bin/
mise activate fish | source
