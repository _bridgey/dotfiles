local wezterm = require('wezterm')

local config = wezterm.config_builder()
local act = wezterm.action
local gpus = wezterm.gui.enumerate_gpus()

config.automatically_reload_config = true

local function create_ssh_domain_from_ssh_config(ssh_domains)
  if ssh_domains == nil then
    ssh_domains = {}
  end
  for host, config in pairs(wezterm.enumerate_ssh_hosts()) do
    table.insert(ssh_domains, {
      name = host,
      remote_address = config.hostname .. ':' .. config.port,
      username = config.user,
      -- multiplexing = "None",
      multiplexing = 'WezTerm',
      assume_shell = 'Posix',
    })
  end
  return ssh_domains
end

local function merge_lists(t1, t2)
  local result = {}
  for _, v in pairs(t1) do
    table.insert(result, v)
  end
  for _, v in pairs(t2) do
    table.insert(result, v)
  end
  return result
end

-- ref: https://zenn.dev/mozumasu/articles/mozumasu-wezterm-customization
-- タブの形をカスタマイズ
-- タブの左側の装飾
local SOLID_LEFT_ARROW = wezterm.nerdfonts.ple_lower_right_triangle
-- タブの右側の装飾
local SOLID_RIGHT_ARROW = wezterm.nerdfonts.ple_upper_left_triangle

wezterm.on('format-tab-title', function(tab, tabs, panes, config, hover, max_width)
  local background = '#5c6d74'
  local foreground = '#FFFFFF'
  local edge_background = 'none'
  if tab.is_active then
    background = '#ae8b2d'
    foreground = '#FFFFFF'
  end
  local edge_foreground = background
  local zoomed = ''
  if tab.active_pane.is_zoomed then
    zoomed = '[Z] '
  end

  local title = '   ' .. zoomed .. wezterm.truncate_right(tab.active_pane.title, max_width - 1) .. '   '
  return {
    { Background = { Color = edge_background } },
    { Foreground = { Color = edge_foreground } },
    { Text = SOLID_LEFT_ARROW },
    { Background = { Color = background } },
    { Foreground = { Color = foreground } },
    { Text = ' ' .. title .. ' ' },
    { Background = { Color = edge_background } },
    { Foreground = { Color = edge_foreground } },
    { Text = SOLID_RIGHT_ARROW },
  }
end)


local function update_ssh_status(window, pane)
  local text = pane:get_domain_name()
  if text == 'local' then
    text = ''
  end
  return {
    { Attribute = { Italic = true } },
    { Text = text .. ' ' },
  }
end

local function display_copy_mode(window, pane)
  local name = window:active_key_table()
  if name then
    name = 'Mode: ' .. name
  end
  return { { Attribute = { Italic = false } }, { Text = name or '' } }
end

wezterm.on('update-right-status', function(window, pane)
  local ssh = update_ssh_status(window, pane)
  local copy_mode = display_copy_mode(window, pane)
  local status = merge_lists(ssh, copy_mode)
  window:set_right_status(wezterm.format(status))
end)

config.font = wezterm.font_with_fallback({
  'UDEV Gothic 35NF',
  'PlemolJP35 Console NF',
  'HackGen35Nerd Console',
})
config.use_ime = true
config.font_size = 12.0
config.color_scheme = 'kanagawabones'
config.window_background_opacity = 0.93
config.animation_fps = 1
config.adjust_window_size_when_changing_font_size = false
config.window_padding = {
  left = 0,
  right = 0,
  top = 0,
  bottom = 0,
}
config.exit_behavior = 'CloseOnCleanExit'
config.webgpu_preferred_adapter = gpus[1]
config.front_end = 'WebGpu'
config.ssh_domains = create_ssh_domain_from_ssh_config()
config.unix_domains = {
  { name = 'unix' },
}
config.default_gui_startup_args = { 'connect', 'unix' }
config.default_prog = { 'fish' }

-- タイトルバーを非表示
config.window_decorations = 'RESIZE'
-- タブが一つの時は非表示
config.hide_tab_bar_if_only_one_tab = true

-- タブの追加ボタンを非表示
config.show_new_tab_button_in_tab_bar = false
-- nightlyのみ使用可能
-- タブの閉じるボタンを非表示
config.show_close_tab_button_in_tabs = false

-- タブ同士の境界線を非表示
config.colors = {
  tab_bar = {
    inactive_tab_edge = 'none',
  },
}

-- keybind
config.disable_default_key_bindings = true
config.leader = { key = 'q', mods = 'CTRL', timeout_milliseconds = 1000 }
config.keys = {
  {
    key = 'v',
    mods = 'LEADER',
    action = act.SplitHorizontal({ domain = 'CurrentPaneDomain' }),
  },
  {
    key = 's',
    mods = 'LEADER',
    action = act.SplitVertical({ domain = 'CurrentPaneDomain' }),
  },
  {
    key = 't',
    mods = 'LEADER',
    action = act.SpawnTab('CurrentPaneDomain'),
  },
  {
    key = 'k',
    mods = 'LEADER|CTRL',
    action = act.CloseCurrentPane({ confirm = false }),
  },
  {
    key = 'k',
    mods = 'LEADER|SHIFT',
    action = act.CloseCurrentTab({ confirm = false }),
  },
  {
    key = 'z',
    mods = 'LEADER',
    action = act.TogglePaneZoomState,
  },
  { key = 'c', mods = 'SHIFT|CTRL', action = act.CopyTo('Clipboard') },
  { key = 'v', mods = 'SHIFT|CTRL', action = act.PasteFrom('Clipboard') },
  { key = 'r', mods = 'SHIFT|CTRL', action = act.ReloadConfiguration },
  { key = 'f', mods = 'SHIFT|CTRL', action = act.Search('CurrentSelectionOrEmptyString') },
  -- { key = 'x', mods = 'SHIFT|CTRL', action = act.ActivateCopyMode },
  { key = '[', mods = 'LEADER', action = act.ActivateCopyMode },
  { key = 's', mods = 'SHIFT|CTRL', action = act.QuickSelect },
  { key = 'p', mods = 'SHIFT|CTRL', action = act.PaneSelect },
  { key = 'u', mods = 'SHIFT|CTRL', action = act.CharSelect },
  { key = 'Tab', mods = 'CTRL', action = act.ActivateTabRelative(1) },
  { key = 'Tab', mods = 'SHIFT|CTRL', action = act.ActivateTabRelative(-1) },
  { key = 'LeftArrow', mods = 'SHIFT|CTRL', action = act.ActivatePaneDirection('Left') },
  { key = 'LeftArrow', mods = 'SHIFT|ALT|CTRL', action = act.AdjustPaneSize({ 'Left', 1 }) },
  { key = 'RightArrow', mods = 'SHIFT|CTRL', action = act.ActivatePaneDirection('Right') },
  { key = 'RightArrow', mods = 'SHIFT|ALT|CTRL', action = act.AdjustPaneSize({ 'Right', 1 }) },
  { key = 'UpArrow', mods = 'SHIFT|CTRL', action = act.ActivatePaneDirection('Up') },
  { key = 'UpArrow', mods = 'SHIFT|ALT|CTRL', action = act.AdjustPaneSize({ 'Up', 1 }) },
  { key = 'DownArrow', mods = 'SHIFT|CTRL', action = act.ActivatePaneDirection('Down') },
  { key = 'DownArrow', mods = 'SHIFT|ALT|CTRL', action = act.AdjustPaneSize({ 'Down', 1 }) },
}

-- mouse
config.mouse_bindings = {
  -- Change the default click behavior so that it only selects
  -- text and doesn't open hyperlinks
  {
    event = { Up = { streak = 1, button = 'Left' } },
    mods = 'NONE',
    action = act.CompleteSelection('ClipboardAndPrimarySelection'),
  },

  -- and make CTRL-Click open hyperlinks
  {
    event = { Up = { streak = 1, button = 'Left' } },
    mods = 'CTRL',
    action = act.OpenLinkAtMouseCursor,
  },
  -- Disable the 'Down' event of CTRL-Click to avoid weird program behaviors
  {
    event = { Down = { streak = 1, button = 'Left' } },
    mods = 'CTRL',
    action = act.Nop,
  },
}

return config

